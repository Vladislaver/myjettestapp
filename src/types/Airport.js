import Location from "./Location";

export default class Airport {
    constructor(name, iataCode, icaoCode, location, comment, natureName, natureLanguage, references, directlyWork) {
        this.name = name || "";
        this.iataCode = iataCode || "";
        this.icaoCode = icaoCode || "";
        this.location = location || new Location();
        this.comment = comment || "";
        this.natureName = natureName || "";
        this.natureLanguage = natureLanguage || "";
        this.references = references || [];
        this.directlyWork = directlyWork || false;
    }
}