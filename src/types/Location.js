export default class Location {
    constructor(country, city) {
        this.country = country || "";
        this.city = city || "";
    }
}