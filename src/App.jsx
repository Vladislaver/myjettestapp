import React from "react";
import ReactDOM from "react-dom";
import CCenterScreenContainer  from "./components/CCenterScreenContainer/CCenterScreenContainer";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlusCircle, faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import CAirportForm from "./components/CAirportForm/CAirportForm";
import Airport from "./types/Airport";
import Location from "./types/Location";


class App extends React.Component {

    constructor() {
        super();

        this.state = {
            airport: new Airport(
                "Vnukovo",
                "VKO",
                "UUWW",
                new Location("Russia", "Moscow"),
                "Description of the subject...",
                "",
                "",
                ["https://en.wikipedia.org/wiki/Vnukovo_International_Airport"],
                true
            )
        };

        library.add(faPlusCircle);
        library.add(faMinusCircle);
    }

    saveAirport(airport) {
        this.setState({
            airport: airport
        });

        this.printAirport(airport);
    }

    printAirport = (airport) => {
        console.log(airport)
    };

    getLanguages = () => {
        return [{
            value: "ru",
            name: "Русский",
        }, {
            value: "en",
            name: "English",
        }]
    };

    render() {
        const {airport} = this.state;

        return (
            <CCenterScreenContainer>
                <CAirportForm
                    airport={airport}
                    languagesList={this.getLanguages()}
                    onSubmit={this.saveAirport.bind(this)}
                />
            </CCenterScreenContainer>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
);
