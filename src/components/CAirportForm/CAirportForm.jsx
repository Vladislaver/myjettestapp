import React from "react";
import classNames from "classnames/bind";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Location from "../../types/Location";
import Airport from "../../types/Airport";
import CInput from "../CInput/CInput";
import CText from "../CText/CText";
import CCheckBox from "../CCheckBox/CCheckBox";
import CSelect from "../CSelect/CSelect";


const style = classNames.bind(require("./CAirportForm.styl"));

export default class CAirportForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: props.airport.name,
            natureName: props.airport.natureName,
            natureLanguage: props.airport.natureLanguage,
            comment: props.airport.comment,
            references: (props.airport.references && props.airport.references.length)
                ? props.airport.references
                : [""],
            directlyWork: props.airport.directlyWork || false,
            invalidFields: [""]
        };
    }

    changeState(stateName, event) {
        this.setState({[stateName]: event.target.type === 'checkbox' ? event.target.checked : event.target.value});
    }

    changeReference(id, event) {
        const references = this.state.references;

        references[id] = event.target.value;
        
        this.setState({references: references.filter(Boolean)});
    }

    removeReference(id) {
        const references = this.state.references;

        if (references.length === 1) {
            references[0] = "";
        } else {
            references.splice(id, 1);
        }

        this.setState({references: references});
    }

    appendReference(id) {
        const references = this.state.references;

        references.splice(id + 1, 0, "");

        this.setState({references: references});
    }

    validate() {
        const invalidFields = [];

        ["name", "natureName", "natureLanguage", "comment"].forEach(fieldName => {
            if (!this.state[fieldName] && !invalidFields.includes(fieldName)) {
                invalidFields.push(fieldName)
            }
        });

        if (!this.state.references || !this.state.references.length || !this.state.references[0]) {
            invalidFields.push("references")
        }

        this.setState({invalidFields: invalidFields});

        return invalidFields.length === 0
    }

    save() {
        if (!this.validate()) {
            return;
        }

        const airport = new Airport();

        airport.name = this.state.name;
        airport.iataCode = this.props.airport.iataCode;
        airport.icaoCode = this.props.airport.icaoCode;
        airport.location = this.props.airport.location;
        airport.comment = this.state.comment;
        airport.natureName = this.state.natureName;
        airport.natureLanguage = this.state.natureLanguage;
        airport.references = this.state.references.filter(Boolean);
        airport.directlyWork = this.state.directlyWork;

        if (this.props.onSubmit) {
            this.props.onSubmit(airport)
        }
    }

    renderLocation = (location) => {
        if (!(location instanceof Location)) {
            return null;
        }

        return [location.city, location.country]
            .filter(Boolean)
            .join(", ");
    };

    render() {
        const {airport, languagesList} = this.props;

        return (
            <div className={style("CAirportForm")}>
                <div className={style("header")}>
                    <h1>Edit Airport "<strong>{airport.name}</strong>"</h1>
                    <div className={style("params")}>
                        <div className={style("param")}>
                            <span className={style("paramName")}>Code IATA:</span>
                            <span className={style("paramValue")}>{airport.iataCode}</span>
                        </div>
                        <div className={style("param")}>
                            <span className={style("paramName")}>Code ICAO:</span>
                            <span className={style("paramValue")}>{airport.icaoCode}</span>
                        </div>
                        <div className={style("param")}>
                            <span className={style("paramName")}>Location:</span>
                            <span className={style("paramValue")}>{this.renderLocation(airport.location)}</span>
                        </div>
                    </div>
                </div>
                <hr/>

                <div className={style("body")}>
                    <div className={style("row")}>
                        <div className={style("leftColumn")}>
                            <span><strong>Airport name</strong><span className={style("required")}>*</span></span>
                        </div>
                        <div className={style("rightColumn")}>
                            <div className={style("valueContainer")}>
                                <div className={style("nameValue")}>
                                    <CInput
                                        value={this.state.name}
                                        onChange={this.changeState.bind(this, "name")}
                                        invalid={this.state.invalidFields.includes("name")}
                                    />
                                </div>
                                <div className={style("natureNameValue")}>
                                    <CInput
                                        value={this.state.natureName}
                                        placeholder="Name in nature language"
                                        onChange={this.changeState.bind(this, "natureName")}
                                        invalid={this.state.invalidFields.includes("natureName")}
                                    />
                                    <CSelect
                                        value={this.state.natureLanguage}
                                        items={languagesList}
                                        placeholder="Nature language"
                                        onChange={this.changeState.bind(this, "natureLanguage")}
                                        invalid={this.state.invalidFields.includes("natureLanguage")}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={style("row")}>
                        <div className={style("leftColumn")}>
                            <span><strong>Comment</strong><span className={style("required")}>*</span></span>
                        </div>
                        <div className={style("rightColumn")}>
                            <div className={style("valueContainer")}>
                                <CText
                                    rows={5}
                                    fullWidth={true}
                                    resize={"vertical"}
                                    value={this.state.comment}
                                    onChange={this.changeState.bind(this, "comment")}
                                    invalid={this.state.invalidFields.includes("comment")}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={style("row")}>
                        <div className={style("leftColumn")}>
                            <span><strong>Reference</strong><span className={style("required")}>*</span></span>
                        </div>
                        <div className={style("rightColumn")}>
                           {this.state.references.map((reference, index) => {
                                return <div key={index} className={style("referenceValue")}>
                                    <div className={style("valueContainer")}>
                                        <CInput
                                            color={"blue"}
                                            fullWidth={true}
                                            value={reference}
                                            invalid={this.state.invalidFields.includes("references")}
                                            onChange={this.changeReference.bind(this, index)}
                                        />
                                    </div>
                                    <button onClick={this.removeReference.bind(this, index)}>
                                        <FontAwesomeIcon icon="minus-circle" color={"grey"} />
                                    </button>
                                    <button onClick={this.appendReference.bind(this, index)}>
                                        <FontAwesomeIcon icon="plus-circle" color={"grey"} />
                                    </button>
                                </div>
                            })}
                        </div>
                    </div>
                    <div className={style("row")}>
                        <div className={style("leftColumn")}>
                            <span><strong>Особенности</strong></span>
                        </div>
                        <div className={style("rightColumn")}>
                            <div className={style("valueContainer")}>
                                <CCheckBox
                                    name={"directlyWork"}
                                    text={"Не работает с операторами напрямую"}
                                    checked={airport.directlyWork}
                                    onChange={this.changeState.bind(this, "directlyWork")}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <button className={style("submitBtn")} onClick={this.save.bind(this)}>Submit</button>
            </div>
        );
    }
}

CAirportForm.defaultProps = {
    airport: new Airport()
};