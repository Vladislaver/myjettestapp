import React from "react";
import classNames from "classnames/bind";

const style = classNames.bind(require("./CCheckBox.styl"));

export default class CCheckBox extends React.Component {

    render() {
        const {name, text, checked, onChange} = this.props;

        return <React.Fragment>
            <input
                className={style("CCheckBox")}
                name={name}
                type="checkbox"
                defaultChecked={checked}
                onChange={onChange}
            />
            <label htmlFor={name}>{text}</label>
        </React.Fragment>
    }
}