import React from "react";
import classNames from "classnames/bind";

const style = classNames.bind(require("./CInput.styl"));

export default class CInput extends React.Component {

    render() {
        const {value, placeholder, onChange, fullWidth, color, invalid} = this.props;

        return <input
            className={style("CInput", {"fullWidth": !!fullWidth}, {"invalid": !!invalid}, color)}
            value={value}
            placeholder={placeholder}
            onChange={onChange}
        />
    }
}