import React from "react";
import classNames from "classnames/bind";

const style = classNames.bind(require("./CText.styl"));

export default class CText extends React.Component {

    render() {
        const {rows, columns, value, onChange, resize, fullWidth, invalid} = this.props;

        return <textarea
            className={style("CText", {"fullWidth": !!fullWidth}, {"invalid": !!invalid}, resize)}
            rows={rows}
            cols={columns}
            value={value || ""}
            onChange={onChange}
        />
    }
}