import React from "react";
import classNames from "classnames/bind";

const style = classNames.bind(require("./CCenterScreenContainer.styl"));

export default class CCenterScreenContainer extends React.Component {

    render() {
        return (
            <div className={style("CCenterScreenContainer")}>
                {this.props.children}
            </div>
        );
    }
}