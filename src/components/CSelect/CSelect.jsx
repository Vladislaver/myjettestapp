import React from "react";
import classNames from "classnames/bind";

const style = classNames.bind(require("./CSelect.styl"));

export default class CSelect extends React.Component {

    render() {
        const {value, items, placeholder, onChange, invalid} = this.props;

        return <select
            className={style("CSelect", {placeholder: !value}, {"invalid": !!invalid})}
            onChange={onChange}
            defaultValue={value}
        >
            <option value="" disabled>{placeholder}</option>
            {items && items.map((item, id) => {
                return <option key={id} value={item.value}>{item.name}</option>
            })}
        </select>
    }
}