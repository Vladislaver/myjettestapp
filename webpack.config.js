"use strict";

const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

function getConfig(isDevMode) {
    return {
        entry: "./src/App.jsx",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "app.bundle.js"
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.styl$/,
                    exclude: /node_modules/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: isDevMode,
                                import: true,
                                modules: true,
                                localIdentName: "[path][name]__[local]--[hash:base64:5]",
                                context: path.resolve(__dirname, "src"),
                                minimize: !isDevMode
                            }
                        },
                        "stylus-loader",
                    ]
                }
            ]
        },
        resolve: {
            extensions: [".js", ".jsx"],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, "src") + "/index.html",
            }),
            new MiniCssExtractPlugin({
                filename: "[name].css",
                chunkFilename: "[id].css"
            })
        ],
        devServer: {
            contentBase: path.join(__dirname, "dist"),
            compress: true,
            port: 9000
        },
        devtool: isDevMode ? "source-map": false
    }
}

module.exports = (env, argv) => {
    return getConfig(argv.mode === "development");
};
